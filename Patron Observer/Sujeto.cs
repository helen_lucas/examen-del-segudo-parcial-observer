﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Patron_Observer
{
    class Sujeto
    {
        private List<IObservador> observadores = new List<IObservador>();
        private string mensajes;
        private Random rnd = new Random();
        private int n;

        public int N { get => n; set => n = value; } 
        public void Suscribir(IObservador suscrito)
        {
            observadores.Add(suscrito);
        }

        public void Desuscribir(IObservador suscrito)
        {
            //se recomienda colocar por seguridad
            observadores.Remove(suscrito);
        }

        private void Notificar()
        {
            // se notifica el nuevo estado a todos los observadores 
            // colocamos a continuacion un foreach
            foreach (IObservador o in observadores)
            {
                o.Update(mensajes);
                o.UpdatePull();

            }     
        }

        public void Trabajo()
        {
            n = rnd.Next(10);
            if (n%2==0)
            {
                Console.ForegroundColor = ConsoleColor.Yellow;
                Console.WriteLine(" Nuevo estado valido");
                mensajes = string.Format(" El nuevo valor es {0}", n);
                Notificar();

            }
        }
    }
}
