﻿using System;

namespace Patron_Observer
{
    class Program
    {
        static void Main(string[] args)
        {
            //creaos el sujeto 
            Sujeto miSujeto = new Sujeto();
            //procedemos a crear los que son los observadores 
            Observador h = new Observador("H", miSujeto);
            Observador g = new Observador("G", miSujeto);
            Observador l = new Observador("L", miSujeto);

            for (int n = 0; n < 5; n++)
                miSujeto.Trabajo();

            //en caso de que alguien se sale de la lista
            Console.WriteLine("---Desuscribir----");

            miSujeto.Desuscribir(g);

            //trabajamos conun for 
            for (int n = 0; n < 5; n++)
                miSujeto.Trabajo();

        }
        
    }
}
